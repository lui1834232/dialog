import { openBlock as o, createElementBlock as l, createElementVNode as c, toDisplayString as a } from "vue";
const s = {
  name: "lUI-dialog"
}, e = /* @__PURE__ */ Object.assign(s, {
  props: {
    title: String
  },
  setup(t) {
    const n = t;
    return (r, p) => (o(), l("div", null, [
      c("h1", null, "Dialog" + a(n.title), 1)
    ]));
  }
}), i = (t) => {
  t.component(e.name, e);
}, _ = {
  install: i
};
export {
  _ as default
};
